<?php
require_once('classes/dbManager.php');

$db = new dbManager();

// check for initial input parameters
if (isset($_REQUEST['hash']) && isset($_REQUEST['playerId']) && isset($_REQUEST['coinsBet'])) {
	
	// return json_encode of result
	echo json_encode($db->spin(substr($_REQUEST['hash'], 0,32), // limit hash to 32 characters  
							   (int) $_REQUEST['playerId'], // cast to int to restrict input
							   (int) $_REQUEST['coinsBet']) // cast to int to restrict input
					);
	
} else { // no hash
	echo '<h1>Incorrect data provided</h1>';
	echo 'Since this is an example application you may try these links: <br />';
	?>
	<p>Player 1:<br />
		<a href="spin.php?hash=<?php echo md5('87654321'); ?>&playerId=1&coinsBet=1">hash=<?php echo md5('87654321'); ?>&playerId=1&coinsBet=1</a><br />
		<a href="spin.php?hash=<?php echo md5('87654321'); ?>&playerId=1&coinsBet=2">hash=<?php echo md5('87654321'); ?>&playerId=1&coinsBet=2</a><br />
		<a href="spin.php?hash=<?php echo md5('87654321'); ?>&playerId=1&coinsBet=5">hash=<?php echo md5('87654321'); ?>&playerId=1&coinsBet=5</a><br />
		<a href="spin.php?hash=<?php echo md5('87654321'); ?>&playerId=1&coinsBet=10">hash=<?php echo md5('87654321'); ?>&playerId=1&coinsBet=10</a>
	</p>
	<p>Player 2:<br />
		<a href="spin.php?hash=<?php echo md5('46791231'); ?>&playerId=2&coinsBet=1">hash=<?php echo md5('46791231'); ?>&playerId=2&coinsBet=1</a><br />
		<a href="spin.php?hash=<?php echo md5('46791231'); ?>&playerId=2&coinsBet=2">hash=<?php echo md5('46791231'); ?>&playerId=2&coinsBet=2</a><br />
		<a href="spin.php?hash=<?php echo md5('46791231'); ?>&playerId=2&coinsBet=5">hash=<?php echo md5('46791231'); ?>&playerId=2&coinsBet=5</a><br />
		<a href="spin.php?hash=<?php echo md5('46791231'); ?>&playerId=2&coinsBet=10">hash=<?php echo md5('46791231'); ?>&playerId=2&coinsBet=10</a>
	</p>
	<?php
}

