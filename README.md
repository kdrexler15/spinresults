# README #

Spin Results example.

### What is this repository for? ###

This project is to demonstrate basic db interaction

### How do I get set up? ###

* Point an Apache virtual host at the root directory of this repo.
* Run db.sql in the root directory to setup db information
* Edit /config/config.php to match your db login information
* Navigate to http://<yoursite>/spin.php 
  The page will provide more information on how to interact with it.