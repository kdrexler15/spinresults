<?php
require_once('config/config.php');

class dbManager {
	
	private $conn;
	
	/**
	 * __construct sets up the db connection
	 */
	public function __construct() {
		try {
			$this->conn = mysql_connect(MYSQL_HOST, MYSQL_USER, MYSQL_PASS, 1);
			mysql_select_db(MYSQL_DB, $this->conn);
		} catch (Exception $e) {
			echo 'No DB Connection!';
			exit;
		}
	}
	
	/**
	 * Handles the spin "action" and logic
	 * @param unknown $hash
	 * @param int $playerId
	 * @param int $coinsBet
	 */
	public function spin($hash, $playerId, $coinsBet) {
		$result = array('PlayerID'=>0,
						'Name'=>null,
						'Credits'=>null,
						'CoinsBet'=>$coinsBet,
						'CoinsWon'=>0,
						'LifetimeSpins'=>null,
						'LifetimeAverageReturn'=>null,
						'Result'=>'SUCCESS',
						'ResultDescription'=>'');
		$playerResult = mysql_query("SELECT * from `player` WHERE md5(`salt_value`) = '".$hash."'", $this->conn);
		
		if (mysql_num_rows($playerResult)==1) {
			// get player db record
			$player = mysql_fetch_assoc($playerResult);
			
			// validate data
			if ($coinsBet<=0) { // no bet
				$result['Result'] = 'ERROR';
				$result['ResultDescription'] = 'Must bet at least one coin!';
				return $result;
			} else if ($coinsBet>10) { // too big of bet
				$result['Result'] = 'ERROR';
				$result['ResultDescription'] = 'Cannot bet more than 10 coins!';
				return $result;
			}
			if ($player['credits']-$coinsBet<0) { // if trying to bet more coins than they have
				$result['Result'] = 'ERROR';
				$result['ResultDescription'] = 'You do not have enough credits!';
				return $result;
			}
			if ($player['id']!=$playerId) { // player id doesn't match
				$result['Result'] = 'ERROR';
				$result['Result'] = 'Incorrect player id!';
				return $result;
			}
			
			// do some calculations, calculate winnings
			$winnings = $this->getSpinAmount();
			$credits = $player['credits']+$winnings-$coinsBet;
			$lifetimeSpins = $player['lifetime_spins'] + 1;
			
			// update db record
			mysql_query("UPDATE `player` set `lifetime_spins`=`lifetime_spins`+1, `credits`=".$credits." where `id`=".$player['id'], $this->conn);
			
			// populate return values since we should be good
			$result['PlayerID'] 			 = $player['id'];
			$result['Name']     			 = $player['name'];
			$result['Credits']  			 = $credits;
			$result['CoinsWon'] 			 = $winnings;
			$result['LifetimeSpins'] 		 = $lifetimeSpins;
			$result['LifetimeAverageReturn'] = number_format($credits/$lifetimeSpins, 2);
		} else { // couldn't find player
			$result['Result'] = 'ERROR';
			$result['ResultDescription'] = 'Could not find player!';
		}
		
		
		return $result;
	}
	
	/**
	 * Generates the winning amount from the spin
	 * @return number
	 */
	private function getSpinAmount() {
		
		// set winning amount
		
		// chance of winning anything (50%)
		$rnd1 = rand(0,1);
		if ($rnd1 == 1) {
			// win 1 through 5
			return rand(1,50);
		} else {
			return 0;
		}
	}
	
	
}