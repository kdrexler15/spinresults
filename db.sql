CREATE SCHEMA `spin_result` ;


CREATE TABLE `spin_result`.`player` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `credits` INT(11) NOT NULL DEFAULT '0',
  `lifetime_spins` INT NOT NULL DEFAULT 0,
  `salt_value` VARCHAR(45) NOT NULL,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC));

-- create a couple of players

INSERT INTO `spin_result`.`player` (
  `id`, `name`, `credits`, `lifetime_spins`, `salt_value`
) VALUES (
  1, 'tester', 0, 0, '87654321'
);

INSERT INTO `spin_result`.`player` (
  `id`, `name`, `credits`, `lifetime_spins`, `salt_value`
) VALUES (
  2, 'tester2', 100, 0, '46791231'
);

commit;